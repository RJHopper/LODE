# What LODE is

LODE stands for Lightweight Open Desktop Environment.

It is a desktop environment that runs common applications efficiently on old hardware despite slow processing speed and/or limited RAM.  

LODE is based on Linux and consists of the Openbox Window manager and with other existing programs(a partial list of these programs exists in packageList.txt above)

LODE has been preconfigured for user-friendliness while minimizing system resource requirements. It is NOT a separate distribution, so it needs an underlying Manjaro Linux system to work. To minimize complexity, it has not been designed to work on any other distros.

# How to install LODE
1. Open a terminal
2. In your home directory(where you should be already), download the scripts with the command: `git clone https://gitlab.com/RJHopper/LODE.git`
3. Go into the downloaded folder with `cd LODE`
4. Make the scripts executable with the command `chmod +x installerator.sh configurator.sh`
5. Run the whole process with `./Setup.sh` (note the dot-slash ('./') preceeding the name. It will not run without that)

The following steps are optional alternatives to step 5, only to be run if you know what you are doing:

5. Run *only* the configuration script with `./configurator.sh` or `./Setup.sh config`
5. Run *only* the installer script with `./installerator.sh` or `./Setup.sh install`

List of commands in order:
```
git clone https://gitlab.com/RJHopper/LODE.git
cd LODE
chmod +x installerator.sh configurator.sh Setup.sh
./Setup.sh
```


# What the code does
The configurator script downloads the configuration files the programs used in LODE need and moved them into the location where they will be needed(the ~/.config)

The installer script then installs the programs required.

The configurator script runs first to avoid creating conflicts with the default configuration files the programs might make when installing.





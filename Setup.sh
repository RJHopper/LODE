#!/bin/bash

case "$1" in
	config) ./configurator.sh ;;
	install) installerator.sh ;;
	*) ./configurator.sh && ./installerator.sh ;;
esac
